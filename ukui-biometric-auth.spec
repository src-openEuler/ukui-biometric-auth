Name:		ukui-biometric-auth	
Summary:	ukui-biometric-auth
Version:	4.0.0.0
Release:	2

License:	GPL-3+
URL:		https://github.com/ukui/ukui-biometric-auth
Source0:	%{name}-%{version}.tar.gz

BuildRequires: cmake qt5-qtbase-devel pam-devel polkit-qt5-1-devel qt5-qttools-devel pkg-config opencv qt5-qtsvg-devel glib2-devel gsettings-qt-devel
BuildRequires: libkysdk-sysinfo-devel
BuildRequires: ukui-interface


%description
ukui-biometric-auth


%package -n libpam-biometric
Summary:	Insertable authentication module for PAM
Suggests: biometric-auth

%description -n libpam-biometric
The indispensable part for biometric authentication in
ukui desktop environment.
This package contains a modules for PAM.


%package -n 	ukui-polkit
Summary:	UKUI authentication agent for PolicyKit-1
Provides: polkit-1-auth-agent
Suggests: biometric-auth

%description -n ukui-polkit
The ukui-polkit package supports general authentication and
biometric authentication that the service is provided by the
biometric-auth package.


%prep
%setup -q

%build
%cmake
%{cmake_build}

%install
%cmake_install

mkdir -p %{buildroot}%{_mandir}/man1/
install -m0644 man/bioauth.1 %{buildroot}%{_mandir}/man1/bioauth.1
install -m0644 man/bioctl.1 %{buildroot}%{_mandir}/man1/bioctl.1
install -m0644 man/biodrvctl.1 %{buildroot}%{_mandir}/man1/biodrvctl.1


%files  -n libpam-biometric
%{_datadir}/ukui-biometric/ukui-biometric.conf
%{_datadir}/dbus-1/system-services/org.ukui.UniauthBackend.service
%{_sysconfdir}/dbus-1/system.d/org.ukui.UniauthBackend.conf
/lib/security/*
%{_bindir}/bioauth
%{_bindir}/bioctl
%{_bindir}/bioctl-helper
%{_bindir}/biodrvctl
%{_bindir}/biorestart
%{_bindir}/uniauth-backend
%{_datadir}/pam-configs/*
%{_datadir}/polkit-1/actions/*.policy
%{_datadir}/ukui-biometric/images/
%{_datadir}/ukui-biometric/i18n_qm/*.qm
%{_datadir}/ukui-biometric/i18n_qm/bioauth-bin/*.qm
%{_mandir}/man1/bioauth.1*
%{_mandir}/man1/bioctl.1*
%{_mandir}/man1/biodrvctl.1*

%files -n ukui-polkit 
%{_sysconfdir}/xdg/*
%{_prefix}/lib/ukui-polkit
%{_prefix}/share/ukui-biometric/i18n_qm/polkit/*.qm
%{_datadir}/applications/polkit-ukui-authentication-agent-1.desktop

%changelog
* Tue Nov 26 2024 Funda Wang <fundawang@yeah.net> - 4.0.0.0-2
- adopt to new cmake macro

* Tue Apr 02 2024 douyan <douyan@kylinos.cn> - 4.0.0.0-1
- Type:update
- ID:NA
- SUG:NA
- DESC: update to upstream version 4.0.0.0-ok1~0426

* Thu May 25 2023 peijiankang <peijiankang@kylinos.cn> - 1.2.4-2
- update glib2 error

* Mon May 22 2023 huayadong <huayadong@kylinos.cn> - 1.2.4-1
- update to upstream version 1.2.4-1

* Wed Mar 01 2023 peijiankang <peijiankang@kylinos.cn> - 1.2.1-13
- add debug to fix not strip

* Mon Dec 19 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-12
- Username to long and ends the display

* Thu Aug 04 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-11
- control file add opencv complication dependencies

* Wed Aug 03 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-10
- can't exit with ctrl-c using biometric autentication 

* Tue Aug 02 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-9
- fix the problem that the probability window is not displayed in the center of screen

* Tue Aug 02 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-8
- solve the problem that ukui-greeter-wayland can't use biometric authentication

* Mon Aug 01 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-7
- modify pam module

* Mon Aug 01 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-6
- add and use attribute of UseHighDpiPixmaps

* Mon Aug 01 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-5
- add window icon

* Sat Apr 02 2022 tanyulong <tanyulong@kylinos.cn> - 1.2.1-4
- add yaml file

* Tue Feb 22 2022 douyan <douyan@kylinos.cn> - 1.2.1-3
- fix libpam-biometric uninstall error

* Wed Oct 20 2021 douyan <douyan@kylinos.cn> - 1.2.1-2
- modify spec file

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 1.2.1-1
- update to upstream version 1.2.1-1

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 1.2.0-1
- Init package for openEuler
